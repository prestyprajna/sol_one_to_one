﻿using Sol_One_To_One.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_One_To_One
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async() =>
            {
                try
                {
                    IEnumerable<IUserEntity> userEntityObj = await new UserDal().GetUserData();

                    foreach (IUserEntity val in userEntityObj)
                    {
                        Console.WriteLine(val?.UserId);
                        Console.WriteLine(val?.FirstName);
                        Console.WriteLine(val?.LastName);
                        Console.WriteLine(val?.userLoginEntityObj?.Username);
                        Console.WriteLine(val?.userLoginEntityObj?.Password);
                        Console.WriteLine(val?.userCommunicationEntity?.MobileNo);
                        Console.WriteLine(val?.userCommunicationEntity?.EmailId);

                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
               
            }).Wait();
            
        }
    }
}
