﻿using Sol_One_To_One.EF;
using Sol_One_To_One.Entity;
using Sol_One_To_One.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_One_To_One
{
    public class UserDal
    {
        #region  declaration
        private UserDBEntities db = null;
        #endregion

        #region  constructor

        public UserDal()
        {
            db = new UserDBEntities();
        }

        #endregion

        #region  public methods

        public async Task<IEnumerable<IUserEntity>> GetUserData()
        {
            return await Task.Run(() =>
            {
                try
                {
                    var getQuery =
                db
                ?.tblUsers
                ?.AsEnumerable()
                ?.Select((leTblUserObj) => new UserEntity()
                {
                    UserId = leTblUserObj?.UserId,
                    FirstName = leTblUserObj?.FirstName,
                    LastName = leTblUserObj?.LastName,
                    userLoginEntityObj = new UserLoginEntity()
                    {
                        Username = leTblUserObj?.tblUserLogin?.Username,
                        Password = leTblUserObj?.tblUserLogin?.Password
                    },
                    userCommunicationEntity = new UserCommunicationEntity()
                    {
                        MobileNo = leTblUserObj?.tblUserCommunication?.MobileNo,
                        EmailId = leTblUserObj?.tblUserCommunication?.EmailId
                    }
                }).ToList();


                    return getQuery;
                }
                catch (Exception)
                {

                    throw;
                }
                
            });
        }

        #endregion
    }
}
