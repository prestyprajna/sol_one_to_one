﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_One_To_One.Entity.Interface
{
    public interface IUserCommunicationEntity
    {
         decimal UserId { get; set; }

         string MobileNo { get; set; }

         string EmailId { get; set; }
    }
}
