﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_One_To_One.Entity.Interface
{
    public interface IUserEntity
    {
         decimal? UserId { get; set; }

         string FirstName { get; set; }

         string LastName { get; set; }

         IUserLoginEntity userLoginEntityObj { get; set; }

         IUserCommunicationEntity userCommunicationEntity { get; set; }
    }

}
