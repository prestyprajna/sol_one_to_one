﻿using Sol_One_To_One.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_One_To_One.Entity
{
    public class UserCommunicationEntity: IUserCommunicationEntity
    {
        public decimal UserId { get; set; }

        public string MobileNo { get; set; }

        public string EmailId { get; set; }
    }
}
